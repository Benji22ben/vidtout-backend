const asyncHandler = require('express-async-handler');
const VideGrenier = require('../models/videGrenierModel');
const User = require('../models/userModel');


// @desc    Get Vide Grenier
// @route   GET /api/videGrenier
// @access  Private
const getVideGrenier = asyncHandler(async (req, res) => {
    // Finding every Vide Grenier in DB
    const videGreniers = await VideGrenier.find({user: req.user.id});

    res.status(200).json(videGreniers);
});

// @desc    Get all Vide Grenier
// @route   GET /api/videGreniers
// @access  Private
const getAllVideGreniers = asyncHandler(async (req, res) => {
    // Finding every Vide Grenier in DB
    const videGreniers = await VideGrenier.find();

    res.status(200).json(videGreniers);
});

// @desc    Creating a new Vide Grenier
// @route   Post /api/videGrenier
// @access  Private
const setVideGrenier = asyncHandler(async (req, res) => {
    if (!req.body) {
        res.status(400)
        
        throw new Error('Request is empty');
    }

    const videGrenier = await VideGrenier.create({
        user: req.user.id,
        name: req.body.name,
        location: req.body.location,
        picture: req.body.picture,
        description: req.body.description,
        tags: req.body.tags,
        startDate: req.body.startDate,
        endDate: req.body.endDate
    });

    res.status(200).json(videGrenier)
});

// @desc    Delete Vide Grenier
// @route   Delete /api/videGrenier
// @access  Private
const deleteVideGrenier = asyncHandler(async (req, res) => {
    const videGrenier = await VideGrenier.findByIdAndUpdate(req.params.id)
    
    if(!videGrenier) {
        res.status(404)
        throw new Error('Vide Grenier not found')
    }


    // Check if user exists
    if(!req.user) {
        res.status(404)
        throw new Error('User not found')
    }

    // Check if user is the owner of the videGrenier
    if(videGrenier.user.toString() !== req.user.id) {
        res.status(401)
        throw new Error('User not authorized')
    }

    const deletedVideGrenier = await VideGrenier.findByIdAndDelete(req.params.id);

    res.status(200).json(deletedVideGrenier);
});

// @desc    Update Vide Grenier
// @route   Put /api/videGrenier
// @access  Private
const updateVideGrenier = asyncHandler(async (req, res) => {
    const videGrenier = await VideGrenier.findByIdAndUpdate(req.params.id)
    
    if(!videGrenier) {
        res.status(404)
        throw new Error('Vide Grenier not found')
    }


    // Check if user exists
    if(!req.user) {
        res.status(404)
        throw new Error('User not found')
    }

    // Check if user is the owner of the videGrenier
    if(videGrenier.user.toString() !== req.user.id) {
        res.status(401)
        throw new Error('User not authorized')
    }

    const updatedVideGrenier = await VideGrenier.findByIdAndUpdate(req.params.id, req.body, {
        new: true,})
    res.status(200).json(updatedVideGrenier)
}); 

module.exports = {
    getVideGrenier,
    getAllVideGreniers,
    setVideGrenier,
    deleteVideGrenier,
    updateVideGrenier
};