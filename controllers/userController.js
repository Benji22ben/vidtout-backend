const jwt = require('jsonwebtoken');
const bcypt = require('bcryptjs');
const asyncHandler = require('express-async-handler')
const User = require('../models/userModel');


// @desc    Creating a new User
// @route   Post /api/users
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
    const { name, email, password } = req.body

    if(!name || !email || !password) {
        res.status(400)
        throw new Error('Please provide all required fields')
    }

    // Check if user already exists
    const userExists = await User.findOne({ email })

    if(userExists) {
        res.status(400)
        throw new Error('User already exists')
    }

    // Hash password
    const salt = await bcypt.genSalt(10)
    const hashedPassword = await bcypt.hash(password, salt)

    // Create user
    const user = await User.create({
        name,
        email,
        password: hashedPassword
    })

    if(!user) {
        res.status(500)
        throw new Error('Something went wrong')
    }
    else{
        res.status(201).json({
            success: true,
            _id: user._id,
            name: user.name,
            email: user.email,
        })
    }
})

// @desc    Authenticate a User
// @route   Post /api/users/login
// @access  Public
const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body

    // Check for user email
    const user = await User.findOne({ email })

    if(user && await bcypt.compareSync(password, user.password)) {
        res.status(200).json({
            success: true,
            token: generateToken(user.id),
            user: {
                _id: user._id,
                name: user.name,
                email: user.email
            }
        })
    }
    else{
        res.status(401).json({
            success: false,
            message: 'Invalid credentials'
        })
    }
})

// @desc    Get user data
// @route   Get /api/users/me
// @access  Private
const getMe = asyncHandler(async (req, res) => {
    res.status(200).json(req.user)
})

// Generate JWT
const generateToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, { 
        expiresIn: '2h' 
    })
}

module.exports = { registerUser, loginUser, getMe };