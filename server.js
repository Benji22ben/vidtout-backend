const express = require('express')
const app = express()
const PORT = 8080
const { errorHandler } = require('./middleware/errorMiddleware')
const connectDB = require('./config/db')

connectDB()

app.use(express.json())

app.use(express.urlencoded({ extended: false }))

app.use('/api/vide-greniers', require('./routes/videGrenierRoutes'))

app.use('/api/users', require('./routes/userRoutes'))

app.use(errorHandler);

app.listen(PORT, function () {
    console.log(`Server Listening on ${PORT}`)
});