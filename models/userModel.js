const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'User name is required']
    },
    email: {
        type: String,
        required: [true, 'User email is required']
    },
    password: {
        type: String,
        required: [true, 'User password is required']
    },
    profile_picture: {
        type: String,
    }
},
{
    timestamps: true
}
);

module.exports = mongoose.model('User', userSchema);