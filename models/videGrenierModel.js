const mongoose = require('mongoose');

const videGrenierSchema = mongoose.Schema({
        user: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'User is required'],
            ref: 'User'
        },
        name: {
            type: String,
            required: [true, 'Vide Grenier name is required']
        },
        location: {
            type: String,
            required: [true, 'Location is required']
        },
        picture: {
            type: String,
        },
        description: {
            type: String,
            required: [true, 'Vide Grenier text is required']
        },
        tags: {
            type: [String],
            required: [true, 'Vide Grenier tags is required']
        },
        startDate: {
            type: Date,
            required: [true, 'Vide Grenier start date is required']
        },
        endDate: {
            type: Date,
            required: [true, 'Vide Grenier end date is required']
        },
    },
    {
        timestamps: true
    }
)

module.exports = mongoose.model('VideGrenier', videGrenierSchema);