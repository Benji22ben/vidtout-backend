const express = require('express');
const router = express.Router();
const {
    getVideGrenier,
    getAllVideGreniers,
    setVideGrenier,
    deleteVideGrenier,
    updateVideGrenier
} = require('../controllers/videGrenierController');
const { protect } = require('../middleware/authMiddleware');

router.route ('/').get(getAllVideGreniers).post(protect, setVideGrenier);

router.route('/user').get(protect, getVideGrenier)

router.route('/:id').delete(protect, deleteVideGrenier).put(protect, updateVideGrenier);


module.exports = router;
