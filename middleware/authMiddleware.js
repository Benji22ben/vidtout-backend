const jwt = require('jsonwebtoken');
const asyncHandler = require("express-async-handler")
const User = require("../models/userModel");

const protect = asyncHandler(async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    try {
        // Get token from header
        token = req.headers.authorization.split(" ")[1]

        // Verify token
        const decoded = jwt.verify(token, process.env.JWT_SECRET)

        // Check if user still exists
        req.user = await User.findById(decoded.id).select("-password")

        next();
    }
    catch (err) {
        // Return error
        res.status(401).json({
            success: false,
            message: "Token is not valid"
        });
    }
  }

  // Check for token   
  if (!token) {
    return res.status(401).json({
      success: false,
      error: "Access denied. No token provided."
    })
  }
})

module.exports = { protect };



