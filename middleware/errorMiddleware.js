const errorHandler = (err, req, res, next) => {
    let status = err.statusCode || 500;
    let message = err.message;
    let data = err.data;
    res.status(status).json({
        message: message,
        data: data,
        stack: process.env.NODE_ENV === 'production' ? '🥞' : err.stack
    });
}

module.exports = {
    errorHandler,
}